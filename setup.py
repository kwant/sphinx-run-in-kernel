from setuptools import setup, find_packages

# Loads version.py module without importing the whole package.
def get_version_and_cmdclass(package_path):
    import os
    from importlib.util import module_from_spec, spec_from_file_location
    spec = spec_from_file_location('version',
                                   os.path.join(package_path, '_version.py'))
    module = module_from_spec(spec)
    spec.loader.exec_module(module)
    return module.__version__, module.cmdclass


version, cmdclass = get_version_and_cmdclass('ipyexecute')

setup(
    name='sphinx-ipyexecute',
    description='Sphinx extension for executing code in an ipython kernel',
    version=version,
    cmdclass=cmdclass,
    packages=find_packages('.'),
)
