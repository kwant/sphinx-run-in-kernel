test
----

1
=

``:hide-output:``

.. execute::
  :hide-output:

  x=1
  print(x)

2
=

.. execute::

  print(x+1)

3
=

``:code-below:`` option

.. execute::
  :code-below:

  1/0
  # Code below output

4
=

With ``:hide-code:`` option

.. execute::
  :hide-code:

  from IPython.display import HTML
  HTML('<h1>source hidden!</h1>')

5
=

.. execute::

  %matplotlib inline
  %config InlineBackend.figure_format = 'svg'
  from matplotlib import pyplot
  pyplot.plot([0, 1])

6
=

Here we start a new kernel

.. execute::
  :new-kernel:

  x

.. execute::

  %matplotlib inline
  from matplotlib import pyplot
  pyplot.plot([1, 0])

.. execute::

  import IPython
  IPython.display.Latex(r"\int_0^\infty f(x) dx")


.. code-block:: ipython

  In [1]: 2**2
  Out[1]: 4
